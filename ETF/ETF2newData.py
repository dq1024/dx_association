# -*- coding: utf-8 -*-
import math
import string

import openpyxl
from datetime import date
import xlrd

# 获取今天的日期
today = date.today()

# 使用strftime生成格式化的日期字符串，不包含分隔符
formatted_date = today.strftime('%Y%m%d')

# 打开Excel文件
excel_file = 'ETF基金' + formatted_date
excel_file_path = 'ETF基金'+ formatted_date +'.xls'  # 替换为您的Excel文件路径
# excel_file_path = 'ETF基金20240325.xls'

workbook = xlrd.open_workbook(excel_file_path, 'w', encoding_override='gbk')
# 选择要处理的工作表
sheet = workbook.sheet_by_name(excel_file)  # 导出的sheet名与文件名一致
# sheet = workbook.sheet_by_name("ETF基金20240325")

# 读取txt文件中的字段
txt_file_path = 'etf_codes.txt'  # 替换为您的txt文件路径

with open(txt_file_path, 'r') as txt_file:
    fields = [line.strip() for line in txt_file]

# 根据字段定位Excel表中的A列
for field in fields:
    for row_index in range(1,sheet.nrows):
        code = sheet.cell_value(row_index, 0)
        newcode=code[:6]
        if newcode == field:  # 假设字段在A列
            q_value = sheet.cell_value(row_index, 16)  # 假设Q列在第17列（从0开始计数）
            processed_value = q_value / 10000
            # print(f"{field}\t{processed_value:.6f}")  # 使用Tab符号分隔
            print(f"{processed_value:.6f}\t",end='')

# 关闭Excel文件
workbook.release_resources()
