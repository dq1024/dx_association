from flask import Flask, render_template, session, redirect, url_for, flash
from flask import make_response
from flask import redirect
from flask import abort
from flask import request
# from flask.ext.bootstrap import Bootstrap
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from datetime import datetime
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import EqualTo, DataRequired
import os
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager

app = Flask(__name__)
app.config['SECRET_KEY'] = 'hard to guess string'

manager = Manager(app)

bootstrap = Bootstrap(app)
moment = Moment(app)


class NameForm(FlaskForm):
    name = StringField('What is your name？', validators=[DataRequired()])
    submit = SubmitField('Submit')


@app.route('/',methods=['GET','POST'])
def index():
    name = None
    form = NameForm()
    if form.validate_on_submit():
        old_name = session.get('name')
        if old_name is not None and old_name != form.name.data:
            flash('Looks like you have changed your name!')
        return redirect(url_for('index'))
    return render_template('index.html', form=form,name=session.get('name'))

#     # user_agent = request.headers.get('User-Agent')
#     # return '<p>Your browser is %s' % user_agent
#     # response = make_response('<h1>This doc carries a cookie! <h1>')
#     # response.set_cookie('answer','41')
#     # return response
#     # return redirect('http://www.baidu.com')


@app.route('/user/<name>')
def user(name):
    # return '<h1>Hello,%s!</h>' % name
    return render_template('user.html', name=name)


@app.route('/user1/<id>')
def get_user(id):
    # user = load_user(id)
    user1 = id
    if not user:
        abort(404)
    return '<h1>Hello,%s!' % id


@app.route('/req')
def index1():
    return '<h1>Bad Request<h1>', 400

@app.route('/jump')
def jump():
    return url_for('index',page=2)


@app.errorhandler(404)
def get_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def get_not_found(e):
    return render_template('500.html'), 500


if __name__ == '__main__':
    print(app.url_map)
    # app.run(debug=True)
    manager.run()
